package wl.cache.util.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.opencsv.bean.CsvToBeanBuilder;

import wl.cache.util.controller.MovieController;
import wl.cache.util.model.Movie;

/*
 * Problem: when readCsvFile is called from getByTest, caching mechanism is not applied.
 * Explanation: https://stackoverflow.com/questions/16899604/spring-cache-cacheable-not-working-while-calling-from-another-method-of-the-s
 */

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MovieService {

	private static final Logger logger = LogManager.getLogger(MovieService.class);
	private final MovieService _movieService;

	@Autowired
	public MovieService(MovieService movieService) {
		_movieService = movieService;
	}

	// https://stackoverflow.com/questions/41534493/spring-boot-enablecaching-and-cacheable-annotation-not-working
	@Cacheable("cacheMovies")
	public List<Movie> readCsvFile() throws IllegalStateException, IOException {
		logger.info("cache : cacheMovies");
		String fileName = "classpath:movieData.csv";
		//File csvFile = ResourceUtils.getFile(fileName);
		//File csvFile = new ClassPathResource(fileName).get;
		BufferedReader bufferedReader = new BufferedReader(
		          new InputStreamReader(new ClassPathResource(fileName).getInputStream()));
		
		List<Movie> beans = new CsvToBeanBuilder<Movie>(bufferedReader).withType(Movie.class)
				.build().parse();
		return beans;
	}

	@Cacheable("cacheByTest")
	public List<Movie> getByTest(String test) throws IllegalStateException, IOException {
		logger.info("cache : cacheByTest(" + test + ")");
		List<Movie> allMovies = _movieService.readCsvFile();
		return allMovies.stream().filter(n -> n.getTest().equalsIgnoreCase(test)).collect(Collectors.toList());
	}

	@Cacheable("cacheByYear")
	public List<Movie> getByYear(int year) throws IllegalStateException, IOException {
		logger.info("cache : getByYear(" + year + ")");
		List<Movie> allMovies = _movieService.readCsvFile();
		return allMovies.stream().filter(n -> n.getYear() == year).collect(Collectors.toList());
	}

	@Cacheable("cacheByFilter")
	public List<Movie> getByFilter(int year, String test) throws IllegalStateException, IOException {
		logger.info("cache : getByFilter(" + year + "," + test + ")");
		List<Movie> allMovies = _movieService.readCsvFile();
		return allMovies.stream()
				.filter(n -> n.getYear() == year && n.getTest().equalsIgnoreCase(test))
				.collect(Collectors.toList());
	}

}
