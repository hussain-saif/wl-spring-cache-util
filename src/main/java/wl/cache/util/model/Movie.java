package wl.cache.util.model;

public class Movie {

	private int year;
	private String imdb;
	private String title;
	private String test;
	
	public Movie() {
		// TODO Auto-generated constructor stub
	}

	
	public Movie(int year, String imdb, String title, String test) {
		super();
		this.year = year;
		this.imdb = imdb;
		this.title = title;
		this.test = test;
	}


	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getImdb() {
		return imdb;
	}

	public void setImdb(String imdb) {
		this.imdb = imdb;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
	
	
	
	
}
