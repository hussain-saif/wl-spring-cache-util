package wl.cache.util.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wl.cache.util.model.Movie;
import wl.cache.util.service.MovieService;

@RestController
public class MovieController {
	
	private static final Logger logger = LogManager.getLogger(MovieController.class);
	
	@Autowired
	MovieService movieService; 
	
	@RequestMapping("/hello")
	//@Cacheable(value = "cacheMovies")
	public String hello() throws IllegalStateException, FileNotFoundException {
		return "Hellooo";
	}
	
	@RequestMapping("/listMovie")
	//@Cacheable(value = "cacheMovies")
	public List customerInformation() throws IllegalStateException, IOException {
		logger.info("customer information from cache");
		List<Movie> movies= movieService.readCsvFile();
		return movies;
	}
	
	@RequestMapping("/movieByTest")
	public List<Movie> getByTest(String test) throws IllegalStateException, IOException {
		logger.info("movieByTest");
		List<Movie> movies= movieService.getByTest(test);
		return movies;
	}
	
	@RequestMapping("/movieByYear")
	public List<Movie> getByTest(int year) throws IllegalStateException, IOException {
		logger.info("movieByYear");
		List<Movie> movies= movieService.getByYear(year);
		return movies;
	}
	
	@RequestMapping("/movieByFilter")
	public List<Movie> getByFilter(int year, String test) throws IllegalStateException, IOException {
		logger.info("movieByFilter");
		List<Movie> movies= movieService.getByFilter(year, test);
		return movies;
	}
	
}
