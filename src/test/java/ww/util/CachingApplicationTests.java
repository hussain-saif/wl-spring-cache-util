package ww.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.collections.comparators.ComparableComparator;
import org.junit.jupiter.api.Test;
import java.util.Comparator;
import java.util.HashSet;

class CachingApplicationTests {

	// @Test
	public void test1() {
		System.out.println("Testing caching application");
		assert (true);
	}

	// @Test
	public void test2() {
		System.out.println("Testing caching application - try 2");
		assert (true);
	}

	// @Test
	public void test3() {
		int[] arr = { -1, 3, 6, 4, 0, 2, 5 };
		// System.out.println(solution(arr));

		Arrays.parallelSort(arr);
		int leastValue = arr[0];
		int maxValue = arr[arr.length - 1];

		List<Integer> generatedList = IntStream.rangeClosed(leastValue, maxValue).boxed().collect(Collectors.toList());
		List<Integer> inputs = Arrays.stream(arr).boxed().collect(Collectors.toList());

		System.out.println(intersectionSimple(inputs, generatedList));
	}

	public int postValidationAndProcess(int result) {
		return (result <= 0) ? 1 : result;
	}

	public BiPredicate<List<Integer>, Integer> containsNumber = (arr, num) -> arr.contains(num);

	public Integer intersectionSimple(List<Integer> a, List<Integer> generatedList) {

		return generatedList.stream().filter(num -> !containsNumber.test(a, num)).findFirst().get();
	}

	@Test
	public void solution() {
		// write your code in Java SE 8
		int num = 0;

		// - convert the N to string
		// - but digit 5 in all possible location
		// - put all combition in Array, and sort the array
		// - return the highest value

		/*
		 * if(num < -8000 || num > 8000){ return num; }
		 */
		
		boolean negativeFlag = false;
		if(num < 0)
		{
			negativeFlag = true;
			num = num * -1;
		}

		// - convert the N to string
		char[] numArr = ("" + num).toCharArray();
		List<Integer> numList = new ArrayList();
		// - but digit 5 in all possible location
		for (int i = 0; i < numArr.length; i++) {
			StringBuffer val = new StringBuffer();
			for (int j = 0; j < numArr.length; j++) {
				if (i == j) {
					val.append("5");
				}
				val.append(numArr[j]);
			}
			if(negativeFlag) {
				numList.add((Integer.valueOf(val.toString())*-1));
			}else {
				numList.add(Integer.valueOf(val.toString()));
			}
			//System.out.println(val.toString());
		}

		// - put all combition in Array, and sort the array
		Collections.sort(numList, Collections.reverseOrder());
		//System.out.println(numList);

		// - return the highest value
		System.out.println("resut :: " + numList.get(0));
		// return numList.get(0);
	}

}
