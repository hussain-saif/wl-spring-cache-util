# - https://gorillalogic.com/blog/build-and-deploy-a-spring-boot-app-on-kubernetes-minikube/
FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD /target/wl-spring-cache-util-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]